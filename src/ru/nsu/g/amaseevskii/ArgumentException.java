package ru.nsu.g.amaseevskii;

public class ArgumentException extends Exception{
    ArgumentException() {
        super("""
                    Wrong arguments:\s
                    format is: flag message tables\s
                    flags:\s
                          -e to encode message\s
                          -d to decode message\s
                          -t to test program with given message (to encode and then decode)\s\s
                    message is a string argument for program to encode/decode/test\s\s
                    tables:\s
                           a set of symbols to use to encode/decode given message\s\s
                           num - numbers (0-9)\s
                           eng - lower case english alphabet\s
                           ENG - upper case english alphabet\s
                           rus - lower case russian alphabet\s
                           RUS - upper case russian alphabet\s
                           Space, tabulation, newline and carriage return will always be added at the end of a table\s\s
                           
                           format is: [table][table]...[table]\s\s
                           example: numengENG\s\s
                           You can use any number of the same tables if you want, don't know why you need it though
                    """);
    }
}
