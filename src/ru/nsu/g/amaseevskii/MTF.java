package ru.nsu.g.amaseevskii;

public class MTF {
    public static String encode(String msg, String symTable) throws Exception {
        StringBuilder output = new StringBuilder();
        StringBuilder s = new StringBuilder(symTable);
        for (char c : msg.toCharArray()) {
            int idx = s.indexOf("" + c);
            if (idx == -1)
                throw new Exception("String to encode has symbols that are not in given table");
            for (int i = 0; i<idx; i++) {
                output.append("1");
            }
            output.append("0");
            s.deleteCharAt(idx).insert(0, c);
        }
        return output.toString();
    }

    public static String decode(String encoded, String symTable) throws Exception {
        StringBuilder output = new StringBuilder();
        StringBuilder s = new StringBuilder(symTable);
        for (int i=0; i< encoded.length(); i++) {
            int idx = 0;
            if (encoded.charAt(i) == '1' || encoded.charAt(i) == '0') {
                while (encoded.charAt(i) == '1') {
                    idx++;
                    i++;
                }
                char c = s.charAt(idx);
                output.append(c);
                s.deleteCharAt(idx).insert(0, c);
            }
            else throw new Exception("String to decode should consist of 0s and 1s");
        }
        return output.toString();
    }


    public static void test(String toEncode, String symTable) {
        String encoded = null;
        try {
            encoded = encode(toEncode, symTable);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(2);
        }
        System.out.println(toEncode + " encoded to: " + encoded);
        String decoded = null;
        try {
            decoded = decode(encoded, symTable);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(3);
        }
        System.out.println(encoded + " decoded to " + decoded);
    }
}
