package ru.nsu.g.amaseevskii;

import static ru.nsu.g.amaseevskii.MTF.*;

public class Main {
    static String toEncode = "";
    static String toDecode = "";

    static StringBuilder symTable = new StringBuilder();
    static String engSymTable = "abcdefghijklmnopqrstuvwxyz";
    static String rusSymTable = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    static String numTable = "0123456789";
    static int mode = 0;

    static boolean eng = false;
    static boolean ENG = false;
    static boolean rus = false;
    static boolean RUS = false;
    static boolean num = false;


    public static void parseArguments(String[] args) {
        if (args.length == 3) {
            switch (args[0]) {
                case "-e" -> {
                    toEncode = args[1];
                    mode = 1;
                }
                case "-d" -> {
                    toDecode = args[1];
                    mode = 2;
                }
                case "-t" -> {
                    toEncode = args[1];
                    mode = 3;
                }
                default -> {
                    try {
                        throw new ArgumentException();
                    } catch (ArgumentException e) {
                        System.out.println(e.getMessage());
                        System.exit(1);
                    }
                }
            }
            StringBuilder tables = new StringBuilder();
            for (int i = 0; i<args[2].length(); i++) {
                tables.append(args[2].charAt(i));
                if ((i + 1) % 3 == 0) {
                    switch (tables.toString()) {
                        case "eng" -> {
                            if (eng)
                                System.out.println("I don't why you need 2 lower case english alphabets, but if you want it, I will add it");
                            symTable.append(engSymTable);
                            eng = true;
                        }
                        case "ENG" -> {
                            if (ENG)
                                System.out.println("I don't why you need 2 upper case english alphabets, but if you want it, I will add it");
                            symTable.append(engSymTable.toUpperCase());
                            ENG = true;
                        }
                        case "rus" -> {
                            if (rus)
                                System.out.println("I don't why you need 2 lower case russian alphabets, but if you want it, I will add it");
                            symTable.append(rusSymTable);
                            rus = true;
                        }
                        case "RUS" -> {
                            if (RUS)
                                System.out.println("I don't why you need 2 upper case english alphabets, but if you want it, I will add it");
                            symTable.append(rusSymTable.toUpperCase());
                            RUS = true;
                        }
                        case "num" -> {
                            if (num)
                                System.out.println("I don't know why you need two sets of 0-9, but if you want it, I will add it");
                            symTable.append(numTable);
                            num = true;
                        }
                        default -> {
                            try {
                                throw new ArgumentException();
                            } catch (ArgumentException e) {
                                System.out.println(e.getMessage());
                                System.exit(1);
                            }
                        }
                    }
                    tables.setLength(0);
                }
            }
            if (eng || ENG || rus || RUS || num)
                symTable.append(" \t\n\r");
        }
        else try {
            throw new ArgumentException();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

    public static void main(String[] args) {

        parseArguments(args);

        switch (mode) {
            case 1:
                try {
                    System.out.println(toEncode + " encoded to: " + encode(toEncode, symTable.toString()));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    System.exit(2);
                }
                break;
            case 2:
                try {
                    System.out.println(toDecode + " decoded to: " + decode(toDecode, symTable.toString()));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    System.exit(3);
                }
                break;
            case 3:
                test(toEncode, symTable.toString());
                break;
        }
    }
}
